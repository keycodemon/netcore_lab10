﻿using ShirtStoreWebsite.Data;
using ShirtStoreWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShirtStoreWebsite.Services
{
    public class ShirtRepository : IShirtRepository
    {
        private ShirtContext _context;

        public ShirtRepository(ShirtContext context)
        {
            _context = context;
        }

        public IEnumerable<Shirt> GetShirts()
        {
            return _context.Shirts.ToList();
        }

        public bool AddShirt(Shirt shirt)
        {
            _context.Shirts.Add(shirt);

            var entries = _context.SaveChanges();
            if (entries > 0)
            {
                return true;
            }

            return false;
        }

        public bool RemoveShirt(int id)
        {
            var shirt = _context.Shirts.SingleOrDefault(m => m.Id == id);
            _context.Shirts.Remove(shirt);

            var entries = _context.SaveChanges();
            if (entries > 0)
            {
                return true;
            }

            return false;
        }
    }
}
